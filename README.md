# Gitlab Webcast

This is a sample webcast page, built by [Jason Cockerham](https://jcock.rocks), as an interview request.

Public URL: http://jcock.gitlab.io/gitlab-webcast/

Concepts: [v1](https://www.figma.com/file/Tzp8AoKM2GuBsKIeAB4HmWvZ/GitLab-Webcast) [v2](https://www.figma.com/file/Tzp8AoKM2GuBsKIeAB4HmWvZ/GitLab-Webcast?node-id=29%3A1) [v3](https://www.figma.com/file/Tzp8AoKM2GuBsKIeAB4HmWvZ/GitLab-Webcast?node-id=26%3A0)

---

## Technology
- Middleman
- Haml
- SCSS
- Bootstrap v4
- jQuery
- jQuery Validate


## Overview
Design/Development Challenge by GitLab

**Project brief:**
Design and develop a landing page promoting an upcoming GitLab webcast. 

The only requirement is to create a sign up form which includes client side validation so that users can receive reminders, and follow up emails after the webcast. 

The rest is up to you. Please host your landing page and provide a url for the hiring team to view, as well as a link to your source code / repository.

## Solution
The site was designed with the GitLab value of **boring solutions** in mind. Too much animation, photos, and artwork for the sake of artwork, were all unneeded for this project. Keeping the main navigation and footer intact, I designed an attractive introduction, flanked by a reminder form. Then, a brief explanation of the primary take-aways, and finally, the ever-present install call-to-action. 

The primary goal of the landing page is to not only introduce the upcoming webcast, but also provide users with an easy way to sign up for convenient reminders and follow up emails. This could also allow GitLab to drop the sign-ups into an advertising pipeline.

---
### Enjoy! 🤘