// DOM ready:
$(function () {
	app.mods.Forms.Init();
});

var app = app || {
	vars: {
	},
	mods: {
		Forms: {
			Init: function () {
				var form = $( 'form' );
				form.validate();

				form.on('submit', function(e) {
					if(form.valid() == true) {
						$('.card--signup').addClass('success');
						$('.form--content').hide();
						$('.thankyou').show();
					}
					return false;
				});
			}
		}
	}
}

